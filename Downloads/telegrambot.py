import logging
import telegram
import os
import os.path
import random
import urllib
from telegram.error import NetworkError, Unauthorized
from time import sleep


def main():
    # Telegram Bot Authorization Token
    bot = telegram.Bot('235245211:AAFvjhxN65WrN_hchkxI2mlbv8FNBsQU7cQ')

    # get the first pending update_id, this is so we can skip over it in case
    # we get an "Unauthorized" exception.
    try:
        update_id = bot.getUpdates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            update_id = echo(bot, update_id)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            # The user has removed or blocked the bot.
            update_id += 1


def echo(bot, update_id):

    # Request updates after the last update_id
    for update in bot.getUpdates(offset=update_id, timeout=10):
        # chat_id is required to reply to any message
        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        message = update.message.text


        if message == '/start':
            # Reply to the message
            bot.sendMessage(chat_id = chat_id, text='hello')


        elif message == '/help':
            # Reply to the message
            bot.sendMessage(chat_id = chat_id, text='i can make your picture like Van Gog '
                                                      'to test that, send me "/van_gog"')

        elif message == '/van_gog':
            # Reply to the message
            bot.sendMessage(chat_id=chat_id, text='now send a picture')
            start_painting(bot, chat_id, update_id)


        elif message:
            # Reply to the message
            bot.sendMessage(chat_id = chat_id, text=message)


    return update_id

def start_painting(bot, chat_id, update_id):
    for update in bot.getUpdates(offset=update_id, timeout=100):

        sample_path = 'img/'
        neural_path = 'neural_artistic_style.py'
        style_path = 'img'

        chat_id = update.message.chat_id
        update_id = update.update_id + 1
        file = bot.getFile(update.message.document.file_id)
        os.makedirs(sample_path + file.file_id)
        urllib.urlretrieve(file.file_path, sample_path + file.file_id + "/" + "pic.jpg")

        bot.sendMessage(chat_id=chat_id, text="drawing...")
        bot.sendChatAction(chat_id=chat_id, action=telegram.ChatAction.TYPING)

        operation = 'python ' + neural_path + \
                    ' --subject ' + sample_path + file.file_id + "/" + "pic.jpg" + \
                    ' --style ' + style_path + '/Vincent.jpg' + \
                    ' --iterations ' + "100" + \
                    ' --output ' + sample_path + file.file_id + "/out.png"
        os.system(operation)

        bot.sendMessage(chat_id=chat_id, text="ready")
        bot.sendPhoto(chat_id=chat_id, photo=open(sample_path + file.file_id + "/out.png", 'rb'))

    return

if __name__ == '__main__':
    main()
